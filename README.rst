[![Docs](https://img.shields.io/badge/docs-latest-brightgreen.svg)](https://docs.attuneautomation.com/)

Attune has a lot of documentation. A high-level overview of how it's organised will help you know where to look for
certain things:

* :ref:`Tutorials<tutorials>` take you by the hand through a series of steps to create jobs. Start here if you're new to Attune or scripting. Also look at the ":ref:`First steps<first_steps>`".
* :ref:`Topic guides<topics>` discuss key topics and concepts at a fairly high level and provide useful background information and explanation.
* :ref:`Reference guides<ref>` contain technical reference of Attune. They describe how it works and how to use it but assume that you have a basic understanding of key concepts.
* :ref:`How-to guides<howto>` are recipes. They guide you through the steps involved in addressing key problems and use-cases. They are more advanced than tutorials and assume some knowledge of how Attune works.