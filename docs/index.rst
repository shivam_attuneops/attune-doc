.. _attune_documentation:

####################
Attune Documentation
####################

Everything you need to know about Attune.

Why Attune
==========

Attune by ServerTribe streamlines and orchestration digital systems with:

* Intuitive script, command, and process automation, mirroring manual inputs.
* Easy development of custom automation solutions.
* Compatibility with popular scripting languages.
* Quick automation process construction.
* IP portability and sharing capabilities.
* Comprehensive multi-server orchestration.
* Centralised scheduling system.
* Automated documentation generation.

Designed for flexibility, Attune serves as a centralised, self-documenting platform for reusable
processes, files, and knowledge. It's versatile enough for enterprise-grade infrastructure and home networks alike.

.. _first_steps:

First steps
===========

Are you new to Attune or to scripting? This is the place to start!

* **From scratch:** :ref:`Getting Started<intro>`
* **Install Attune for free:** `Attune Community Edition <http://download.servertribe.com>`_
* **Tutorial:** :ref:`Linux or macOS<tutorial_linux>`

----

.. _getting_help:

Getting Help
============

Having trouble? We'd like to help!

* Try the :ref:`FAQ<faq>` - it's got answers to many common questions.
* Connect to our `Discord Community <http://discord.servertribe.com>`_.
* Report bugs with Attune in our `ticket tracker <http://issues.servertribe.com>`_.

----

Engage the community
====================

* `Discord <http://discord.servertribe.com>`_
* `YouTube <https://www.youtube.com/@servertribe>`_
* `Github <https://github.com/Attune-Automation>`_

----

How the documentation is organised
==================================

Attune has a lot of documentation. A high-level overview of how it's organised will help you know where to look for
certain things:

* :ref:`Tutorials<tutorials>` take you by the hand through a series of steps to create jobs. Start here if you're new to Attune or scripting. Also look at the ":ref:`First steps<first_steps>`".
* :ref:`Topic guides<topics>` discuss key topics and concepts at a fairly high level and provide useful background information and explanation.
* :ref:`Reference guides<ref>` contain technical reference of Attune. They describe how it works and how to use it but assume that you have a basic understanding of key concepts.
* :ref:`How-to guides<howto>` are recipes. They guide you through the steps involved in addressing key problems and use-cases. They are more advanced than tutorials and assume some knowledge of how Attune works.

----

.. toctree::
    :caption: Documentation Contents
    :titlesonly:

    intro/index
    topics/index
    howto/index
    ref/index
    faq/index
    glossary/glossary
    release_notes/IndexReleaseNotes
