.. _git_ftp_ref:

=======================
git_ftp reference guide
=======================

Git-ftp is an FTP client using Git to determine which local files to upload or which files to delete on the remote host.
For more information, see the git_ftp manual: https://git-ftp.github.io/

Syntax
~~~~~~

.. code-block::

    git ftp <action>[<options>][<url>]

Actions
~~~~~~~

Some of the common options are listed here:

* :code:`init` : Uploads all git-tracked non-ignored files to the remote server and creates the .git-ftp.log file
  containing the SHA1 of the latest commit.

* :code:`push` : Uploads files that have changed and deletes files that have been deleted since the last upload.

* :code:`log` : Downloads last uploaded SHA1 from log and hooks `git log`.

* :code:`help` : Shows the help screen.

Options
~~~~~~~

* :code:`-u [username]`, :code:`--user [username]` : FTP login name. If no argument is given, local user will be taken.

* :code:`-p [password]`, :code:`--passwd [password]` : FTP password. See -P for interactive password prompt. (note)

* :code:`-P`, :code:`--ask-passwd` : Ask for FTP password interactively.

* :code:`-a`, :code:`--all` : Uploads all files of current Git checkout.

* :code:`-c`, :code:`--commit` : Sets SHA1 hash of last deployed commit by option.

* :code:`-A`, :code:`--active` : Uses FTP active mode. This works only if you have either no firewall and a direct connection to the server or an FTP aware firewall. If you don't know what it means, you probably won't need it.

* :code:`-b [branch]`, :code:`--branch [branch]` : Push a specific branch

* :code:`-h`, :code:`--help` : Prints some usage information.

* :code:`-v`, :code:`--verbose` : Be verbose.

* :code:`-vv` : Be as verbose as possible. Useful for debug information.

* :code:`--insecure` : Don't verify server's certificate.

* :code:`--cacert <file>` : Use as CA certificate store. Useful when a server has a self-signed certificate.

* :code:`--disable-epsv` : Tell curl to disable the use of the EPSV command when doing passive FTP transfers. Curl will normally always first attempt to use EPSV before PASV, but with this option, it will not try using EPSV.

* :code:`--auto-init` : Automatically run init action when running push action

URL
~~~

.. code-block::

    protocol://host.domain.tld:port/path

For example

.. code-block::

    ftp://host.example.com:2121/mypath

Supported Protocols are:

* :code:`ftp://...` : FTP (default if no protocol is set)

* :code:`sftp://...` : SFTP

* :code:`ftps://...` : FTPS

* :code:`ftpes://...` : FTP over explicit SSL (FTPES) protocol
