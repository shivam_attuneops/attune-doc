.. _bool_best_practice:

======================
Boolean Best Practices
======================

It is best to use **booleans** in your scripts rather than using an integer to check
for the truth of a statement. The reason for this is that it cleans up the code and
makes it more intuitive when mapping values to parameter.

For example, if a parameter is called **Create Full Backup**, :code:`true` makes more
sense than :code:`1`

Bash
----

:code:`true` and :code:`false` are native to Mac and Linux operating systems,
this means variable can be evaluated without assigning integers.

For example:

.. code-block:: bash
    :linenos:

    if {attuneVar}
    then
        echo "true"
    fi

Attune substitutes the a value into the :code:`{attuneVar}` parameter.

If the value is :code:`true` :

.. code-block:: bash
    :linenos:

    if true
    then
        echo "true"
    fi

.. image:: bash_bool.png

If you want to evaluate a variable as false, you can use the NOT operator :code:`!`

.. code-block:: bash
    :linenos:

    if ! false
    then
        echo 'This is false'
    fi

Multiple values can be evaluated with the AND operator :code:`&&`

.. code-block:: bash
    :linenos:

    if true && ! false
    then
        echo 'The first value is true and the second value is false'
    fi

Powershell
----------

The same is true for Powershell except Powershell use :code:`$true` and :code:`$false`.

For example:

.. code-block:: powershell
    :linenos:

    if (${{attuneVal}}) {
        Write-Host "true"
    }

Attune substitutes the value into the :code:`{attuneVar}` parameter.

If the value is :code:`true` :

.. code-block:: powershell
    :linenos:

    if ($true) {
        Write-Host "true"
    }

.. image:: powershell_bool.png

If you want to evaluate a variable as false, you can use the NOT operator :code:`-not`

.. code-block:: powershell
    :linenos:

    if -not ($false) {
        Write-Host "Not False"
    }



Python
------

Python uses :code:`True` and :code:`False`. To keep the use of booleans consistent
throughout a project, you are able to assign the booleans to variables at the
beginning of a Python script.

For example:

.. code-block:: python
    :linenos:

    true=True
    false=False

    if {attuneVal}:
        print("Do the work for true")
    else
        print("Do the work for else")

If the Parameter is mapped to :code:`true`, Attune substitutes :code:`true` into the :code:`{attuneVal}` parameter
which is assigned to :code:`True`.

.. code-block:: python
    :linenos:

    true=True
    false=False

    if true:
        print("Do the work for true")
    else
        print("Do the work for else")

.. image:: python_bool.png

If you want to evaluate a variable as false, you can use the NOT operator :code:`not`

.. code-block:: python
    :linenos:

    true=True
    false=False

    if not false:
        print("Do the work for true")
    else
        print("Do the work for else")

Multiple values can be evaluated with the AND operator :code:`and`

.. code-block:: python
    :linenos:

    true=True
    false=False

    if not false and true:
        print("Do the work for true")
    else
        print("Do the work for else")
