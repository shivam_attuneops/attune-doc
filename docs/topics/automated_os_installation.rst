.. _automated_os_installation:

######################################
Automate Operating System Installation
######################################

Attune provides a solution to streamline the OS installation process. It works across Windows and 
Linux on many physical or virtual controllers, such as:

* `iDrac <https://www.dell.com/en-au/dt/solutions/openmanage/idrac.htm>`_
* `PiKVM <https://pikvm.org/>`_
* `oVirt <https://www.ovirt.org/>`_
* `HyperV <https://learn.microsoft.com/en-us/virtualization/>`_
* `VirtualBox <https://www.virtualbox.org/>`_
* `Proxmox <https://www.proxmox.com/>`_
* `ESXi <https://www.vmware.com/au/products/esxi-and-esx.html>`_

Or you can create bootable flash drives if you don't have access to a controller.

Getting Started
===============

**Setup the Worker**

A worker is key in creating ISO files. Workers handle tasks. These tasks include unpacking ISO 
files. They also involve adding drivers and settings. 

You will send files to the controller and call APIs. 

The worker can be on macOS, Linux, or Windows. Attune Projects work with Windows, macOS, and 
Linux workers.

Here is a list of the setup projects to clone:

* `Setup Windows to Automate Operating System Installations <https://github.com/Attune-Automation/Setup-Windows-to-Automate-Operating-System-Installations>`_
* `Setup macOS or Linux to Automate Operating System Installation <https://github.com/Attune-Automation/Setup-macOS-or-Linux-to-Automate-Operating-System-Installations>`_

Follow the instructions within the project to setup your worker.

**Understanding the process**

Two parts make up the OS installation automation.

1. :ref:`Creating the ISO files <creating_the_iso>` and
2. :ref:`Building the Machine <building_the_machine>`.

The two parts merged into :ref:`Glue Projects<glue_project>`, performing a unique process.

.. _creating_the_iso:

Creating the ISO(s)
===================

To make an ISO image, set up separate Attune Projects. Each OS and worker type needs Blueprints for 
ISO creation.

The ISO creation process is controller-independent. No controller-specific details needed.

Tasks needing controller info go in Controller APIs Project.

* `Create Windows ISO with autounattend built on Windows <https://github.com/Attune-Automation/Create-Windows-ISO-with-autounattend-built-on-Windows>`_
* `Create Windows ISO with autounattend built on macOS or Linux <https://github.com/Attune-Automation/Create-Windows-ISO-with-autounattend-built-on-macOS-or-Linux>`_
* `Create Red Hat Enterprise Linux RHEL with kickstart built on macOS or Linux <https://github.com/Attune-Automation/Create-Red-Hat-Enterprise-Linux-RHEL-ISO-with-kickstart-built-on-macOS-or-Linux>`_

**Project Naming**

The following way names the projects to create the ISO's.

:code:`Create {os_name} ISO with {method} built on {worker_type}`.

:code:`os_name` examples:

* Red Hat Enterprise Linux RHEL
* Windows
* Ubuntu
* Debian

:code:`method` examples:

* Kickstart
* Preseed
* autoinstall
* autounattend

:code:`worker_type` examples:

* Windows
* macOS or Linux

**Parameters**

This Project will need a worker to build the ISO image. This worker can be
the device Attune is running on or another device. We will name this device:

:code:`Automation Worker {type}`

We will name the parameters for the new operating system that we are creating:

:code:`New OS {type}`

:code:`type` examples:

* Node
* User: {account such as 'root', 'Administrator', or 'non privileged'}
* Base Directory

**Blueprints**

We will name the Blueprint:

:code:`Create {os_distribution} {installation_method} ISO`.

:code:`os_distribution` examples:

* Red Hat Enterprise Linux 8 (RHEL8)
* Windows 10 (Win10)
* Windows Server 2019 (Win2019)

:code:`installation_method` (for Windows only) examples:

* Single
* Dual
* WinPE

**Attaching Drivers**

The device drivers and ISO creation projects work independently. Drivers go into 
a drop directory before ISO creation. When creating an ISO, it copies drivers 
into it.

* `Intel NUC Drivers <https://github.com/Attune-Automation/Intel-NUC-Drivers>`_
* `MSI Drivers <https://github.com/Attune-Automation/MSI-Drivers>`_

Create new projects for your devices drivers and add to this growing list.

**Test and Cleanup**

The Project will include a Blueprint to test the installation. It will also 
include a Blueprint to clean the build files on the worker.

.. _building_the_machine:

Building the Machine
====================

In this section, we'll set up distinct Attune Projects for each controller. 
To ensure Blueprints work on any operating system, we can also make a bootable 
flash disk.

We'll name these projects and specify details. These include the device running 
scripts, the controller, host machine, and build machine. Additionally, we'll 
include information on CPU count and boot ISO directory.

Our goal is to make Blueprints in the Controller APIs Project compatible with 
any OS. Yet, Linux and Windows differ. So, complete OS agnosticism may not always 
be possible.

* `PiKVM APIs from Windows <https://github.com/Attune-Automation/PiKVM-APIs-on-Windows>`_
* `PiKVM APIs from macOS or Linux <https://github.com/Attune-Automation/PiKVM-APIs-on-macOS-or-Linux>`_
* `VMWare ESXi APIs from macOS or Linux <https://github.com/Attune-Automation/VMWare-ESXi-APIs-from-macOS-or-Linux>`_
* `oVirt APIs from macOS or Linux <https://github.com/Attune-Automation/ovirt-APIs-from-macOS-or-Linux>`_
* `Windows Storage cmdlets <https://github.com/Attune-Automation/Windows-Storage-cmdlets>`_

**Project Naming**

The Projects perform processes on machine controllers and virtual hosts. Here are 
their names:

:code:`{controller} APIs from {worker_type}`

:code:`controller` examples:

* iDrac
* PiKVM
* VMWare ESXi

:code:`worker_type` examples:

* Windows
* macOS or Linux

Or, the Project Name for creating bootable USB disks will differ.

**Parameters**

This Project may need a device to run scripts and send commands to 
the controller. This device can be the Attune Server or another 
device. We will name this device: 

:code:`Automation Worker {type}`

Name the controller or host machine: 

:code:`Controller {type}`

We will name the machine that we are going to build or rebuild: 

:code:`Build Machine {type}`

:code:`type` examples:

* Node
* ESXi Host
* vCenter Node
* User: {account such as 'root' or 'non privileged'}
* CPU Count
* Boot ISO Directory

.. _glue_projects:

Putting it all Together (Glue Projects)
=======================================

This last part is about glue projects. They help make Plans for building 
machines easier. The key is setting parameters and removing duplicates.

**Project Naming**

The Projects perform the whole workflow. They install an OS on a machine. 
The following names are:

:code:`{controller} {os_name} Installer from a {worker_type} Machine`

:code:`controller` examples:

* iDrac
* PiKVM
* VMWare ESXi

:code:`os_name` examples:

* Red Hat Enterprise Linux RHEL
* Windows
* Ubuntu
* Debian

:code:`worker_type` examples:

* Windows
* macOS or Linux

**Parameters**

When linking Blueprints into the Glue Project, set Parameters in advance. 
Also, combine any duplicate Parameters.

For an example of a Glue Project:
`PiKVM Windows Installer from a Windows Machine <https://github.com/Attune-Automation/PiKVM-Windows-Installer-from-a-Windows-Machine>`_

Wrapping Up: Streamlining OS Installation
=========================================

This framework automates installing different OS with Attune Projects. It 
covers creating ISO images, setting up machines, and integrating components. 
It focuses on clear naming, scalability, and system-agnostic design. This 
approach simplifies OS installations, making them efficient and reliable. 
Users get automated installations on various hardware and virtual setups.