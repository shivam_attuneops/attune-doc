.. _add_local_user_to_remote_mgmt:

===============================================================================
How To Add Local Non-Administrator Account to the Remote Management Users Group
===============================================================================

To add a local non-administrator account to the "Remote Management Users" group on a Windows machine using 
PowerShell, you can use the `Add-LocalGroupMember` cmdlet.

Add the account, run the following command:

::

    Add-LocalGroupMember -Group "Remote Management Users" -Member "{username}"

Verify that the user has been added to the group by using the `Get-LocalGroupMember`.

::

    Get-LocalGroupMember -Group "Remote Management Users"

Complete
--------
This procedure is now complete.