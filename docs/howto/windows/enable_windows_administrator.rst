.. _enable_windows_administrator:

====================================================
How To Enable Windows Built-in Administrator Account
====================================================

In PowerShell, you can enable the built-in Administrator account on a local machine by using the net user command 
or the LocalUser cmdlet available in the `Microsoft.PowerShell.LocalAccounts` module (available in PowerShell 5.1 
and later). Here's how you can do it using both methods:

Using 'net user' Command
-------------------------

The net user command is a legacy command available in Command Prompt but can also be executed from PowerShell.

Procedure
`````````

Run the following steps in PowerShell with administrative privileges.

Set the desired password, run the following command:

::

    $PlainTextPassword = '{password}'  # Replace with the desired password

Enable the Administrator account, run the following command:

::

    net user Administrator $PlainTextPassword /active:yes

This command sets the built-in Administrator account to active.

Using 'LocalUser' Cmdlet
------------------------

In PowerShell 5.1 and later, you can use the Enable-LocalUser cmdlet from the Microsoft.PowerShell.LocalAccounts 
module to enable the built-in Administrator account.

Procedure
`````````

Run the following steps in PowerShell with administrative privileges.

Set the desired password, run the following command:

::

    $PlainTextPassword = '{password}'  # Replace with the desired password

Enable the Administrator account, run the following command:

::

    # Import the LocalAccounts module
    Import-Module Microsoft.PowerShell.LocalAccounts

    # Enable the built-in Administrator account
    Enable-LocalUser -Name "Administrator"

    # Convert the password to a secure string
    $SecurePassword = ConvertTo-SecureString -String $PlainTextPassword -AsPlainText -Force

    # Set the new password
    Set-LocalUser -Name Administrator -Password $SecurePassword

This cmdlet enables the built-in Administrator account.

Complete
--------
This procedure is now complete. Your Administrator account is enabled.

.. note::
    The built-in Administrator account is usually disabled by default for security reasons. Enabling it can pose a 
    security risk if not managed properly. Always ensure that it's secured with a strong password and that you 
    understand the implications of enabling it.
