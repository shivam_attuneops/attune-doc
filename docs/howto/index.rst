.. _howto:

###############
"How-to" guides
###############

Here you'll find short answers to "How do I...?" types of questions. These how-to guides don't cover topics in depth.
However, these guides will help you quickly accomplish common tasks.

.. toctree::
    :maxdepth: 1

    workspace_design/clone_project
    workspace_plan/create_job
    setup_winrm/setup_winrm_cifs_manually
    setup_winrm/setup_winrm_via_ad
    windows/enable_windows_administrator
    windows/add_local_user_to_remote_mgmt
    snippets/index
    setup_ftpes/setup_ftpes
