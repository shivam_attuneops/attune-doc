.. _setup_ftpes:

======================================
How to configure GIT remotes with FTPS
======================================

This document explains how to configure an Redhat 8 Attune node to use FTP with Explicit for version control.
This method is a way of using the FTP protocol but in a secure way. When FTP makes a TCP connection, it is encrypted end
to end so the data can not be read in transit.


Install
```````

Install openssl
~~~~~~~~~~~~~~~

Check to see if **OpenSSL** is installed on the Attune host:

.. code-block::
    :linenos:

    dnf list installed openssl

If it is not installed, install it with:

.. code-block::
    :linenos:

    dnf install openssl

Install GIT
~~~~~~~~~~~

Git only needs to be installed if it is not already, to check
if git is installed, run: :code:`dnf list installed git`

To install git, run:

.. code-block::

    dnf install git

Install git ftp
~~~~~~~~~~~~~~~

GIT_FTP is required for the FTP Transfer. The project can be found here:
https://github.com/git-ftp/git-ftp

To install git_ftp run:

.. code-block::
    :linenos:

    # as the attune user
    curl https://raw.githubusercontent.com/git-ftp/git-ftp/master/git-ftp > $HOME/bin/git-ftp
    chmod 755 $HOME/bin/git-ftp

This downloads the script and makes it executable in the Attune users binary directory.

Install curl-devel package
``````````````````````````

The curl-devel package includes the header files and libraries necessary to develop applications that handle HTTP or
FTP transactions using the cURL library.

To install the package, run:

.. code-block::
    :linenos:

    dnf install -y curl-devel

Install openssl-devel package
`````````````````````````````

To install the package, run:

.. code-block::
    :linenos:

    #as root
    dnf install -y openssl-devel


Build curl v8
`````````````

git_ftp utilised curl for the ftp transfer. curl Version 8 or greater needs to be installed but this is no always
available in RHEL8 repositories. It is important that this version of curl is installed in the users :code:`bin`
directory so that replacing the system version does not cause anything to break.

Install **Dev Tools**:

.. code-block::
    :linenos:

    sudo yum groupinstall -y "development tools"

----

Download  **curl version 8**:

.. code-block::
    :linenos:

    curl --output bin/curl-8.5.0.tar.gz https://curl.se/download/curl-8.5.0.tar.gz

----

Extract the file:

.. code-block::
    :linenos:

    tar -xvf curl-8.5.0.tar.gz

----

Change into the directory:

.. code-block::
    :linenos:

    cd curl-8.5.0

----

Configure **curl**

.. code-block::
    :linenos:

    ./configure --with-openssl --prefix=$HOME

----

Build **curl**

.. code-block::
    :linenos:

    make

.. warning:: Ensure that the install path is :code:`/home/attune/bin`

----

Install **curl**

.. code-block::
    :linenos:

    make install

----

Add the Attune users bin directory to the **PATH**.

.. code-block::
    :linenos:

    cat <<'EOF' >> ~/.bashrc

    # Add Path for GIT FTP
    export PATH="$HOME/bin:$PATH"

    EOF

----

Source the bashrc file

.. code-block::
    :linenos:

    source ~/.bashrc

----

Check that the version is :code:`v8`

.. code-block::
    :linenos:

    curl --version


.. note:: If the version is not v8, you need to check that :code:`/home/attune/bin` is in the :code:`$PATH` variable.

Configure the Repository
````````````````````````

Configure the git ftp repository settings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In your project repository, run:

.. code-block::
    :linenos:

    git config git-ftp.user "<username>"
    git config git-ftp.url "<URL>"
    git config git-ftp.password "<password>"

.. warning:: It is not secure to store the users password in plain text. git_ftp can use the -P flag to prompt the
    users password.

.. note::

    The url must specify explicit TLS like :code:`ftpes://<hostname>:<port><path_to_file>.git`
    The path is the relative path from the FTP session root.

Run GIT INIT
````````````

Running git init in an existing repository is safe. It will not overwrite things that are already there. The primary
reason for rerunning git init is to pick up newly added template..

For example:

.. code-block::
    :linenos:

    git ftp init -P --disable-epsv -v --insecure "ftpes://10.211.55.2:21/trace-configuration-script.git"

* -P asks the user to manually enter the password
* --disable-epsv Disables Extended Passive Mode
* -- insecure  Does not check the certificate

How to use Git FTP
~~~~~~~~~~~~~~~~~~

To push a file
``````````````

In your repository, run:

.. code-block::
    :linenos:

    git ftp push -P --disable-epsv -v --insecure

Git FTP Reference
~~~~~~~~~~~~~~~~~

For a list of git-ftp reference, see: :ref:`git_ftp_ref`

Troubleshooting
~~~~~~~~~~~~~~~

For troubleshooting instructions, see :ref:`troubleshoot_ftpes`

Appendix
~~~~~~~~

Prerequisites
`````````````

The Prerequisites for FTPES are:

* GIT installed.
* GIT_FTP installed.
* CURL version 8
* OpenSSL is installed
* Curl Devel
