.. _setup_winrm_cifs_manually:

==========================
How To Setup WinRM, CIFS Manually
==========================

Objective
---------
This procedure will setup a Windows desktop or server to allow connections and Automation from Attune. This is done via enabling Windows File Sharing (CIFS) and Windows Remote Management (WinRM).

Procedure
---------

To enable WinRM, run the following command in PowerShell.

::

   # Enable WinRM with HTTPS, via PowerShell
   # This is for windows deployment step
   Enable-PSRemoting -SkipNetworkProfileCheck -Force
   Remove-Item -Path WSMan:\Localhost\listener\listener* -Recurse;
   New-Item -Path WSMan:\LocalHost\Listener `
         -Transport HTTPS `
         -Address * `
         -CertificateThumbPrint `
               (New-SelfSignedCertificate `
               -CertstoreLocation Cert:\LocalMachine\My `
               -DnsName $env:computername `
               -NotAfter (get-date).AddYears(6)).Thumbprint `
      -Force
   Restart-Service -Force WinRM
   New-NetFirewallRule -DisplayName 'WinRM HTTPS' `
                     -Name 'WinRM_HTTPS' `
                     -Profile Any `
                     -LocalPort 5986 `
                     -Protocol TCP

   # Enable CredSSP on Win11, via PowerShell
   $osName = (Get-ComputerInfo).OsName
   if ($osName -like "*Windows 11*") {
      Enable-WSManCredSSP -Role Server
   }

To verify WinRM with HTTPS is enabled, run the following command in PowerShell:

::

      Get-WSManInstance WinRM/Config/Listener -Enumerate | Where-Object { $_.transport -eq "HTTPS" }

To enable file and print sharing, run the following command in PowerShell:

::

    # Enable File and Print sharing, via PowerShell
    # This is for windows deployment step
    Get-NetFirewallRule -DisplayGroup 'File and Printer Sharing' `
        | Set-NetFirewallRule -Profile Any -Enabled true

To verify file and print sharing is enabled, run the following command in PowerShell:

::

      Get-NetFirewallRule -DisplayGroup "File And Printer Sharing" | select DisplayName, Enabled

Complete
--------
This procedure is now complete. Your file and printing sharing is enabled.
