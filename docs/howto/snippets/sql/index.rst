.. _sql_snippets:

==========================
SQL (Oracle) Code Snippets
==========================

.. toctree::
    :maxdepth: 1
    :caption: Contents:

    sqlPlusHumanReadable
    loopOverNodeList
    checkUserGrant
    bashAsOracle
    dynamicOracleBash
    bashQuerySql
