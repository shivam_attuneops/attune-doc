.. _bashAsOracle:

========================================================================
How To Create Bash Script to Perform Oracle Commands as Oracle on Target Server
========================================================================

Option 1:

.. code-block::
    :linenos:

    F=$(mktemp)
    printf "
    alter system set COMPATIBLE='19.3.0' scope=spfile;
    shutdown immediate;
    startup mount;
    alter database open;
    " > ${F}

    {oracleDbHome}/bin/sqlplus / as sysdba @${F}
    rm ${F}

----

Option 2:

.. code-block::
    :linenos:


    # Ensure ORACLE_HOME is set
    # Ensure ORACLE_BIN is set to $ORACLE_HOME/bin
    # Ensure ORACLE_BIN is in $PATH

    which sqlplus

    sqlplus /nolog <<EOF
    CONN {oracleUser.user}/{oracleUser.password}@{oracleUser.sid}
    SELECT * FROM DUAL
    EOF
