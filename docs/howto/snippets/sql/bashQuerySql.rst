.. _bashQuerySql:

============================================
How To Use SQL to Create a File that Bash can Query
============================================

.. code-block::
    :linenos:

    #Query the host_details for the type of host
    sqlplus <user>/<userpassword>@<instance> > $tmpFile << EOF
    SET ECHO OFF
    SET HEADING OFF
    SET PAGES 100
    SET FEEDBACK OFF
    SELECT host_name, host_type
      FROM host_details
      WHERE host_name '$dbname'
      AND host_type IN (1, 4)
    EOF

    # Get the host type from the file
    hostType=$(grep $dbName $tmpFile | awk '{ print $2}' )
