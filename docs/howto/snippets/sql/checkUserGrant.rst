.. _checkUserGrant:

==========================
How To Check the Grants on a User
==========================

Run this query in SQL Developer. You’ll be prompted for the user when you run the query. Alternatively run the query in
**sqlplus** but substitute a string in place of :code:`:the_user` variable.

.. code-block:: sql
    :linenos:

    SELECT GRANTEE, PRIVILEGE
        FROM sys.dba_sys_privs
        WHERE grantee = :the_user
    UNION
    SELECT GRANTEE, PRIVILEGE
        FROM dba_role_privs rp JOIN role_sys_privs rsp ON (rp.granted_role = rsp.role)
        WHERE rp.grantee = :the_user;

