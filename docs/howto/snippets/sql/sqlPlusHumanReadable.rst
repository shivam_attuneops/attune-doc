.. _sqlPlusHumanReadable:

===============================
How To Get Readable Results in sqlplus
===============================

Use SQL Developer wherever possible. It is much better than sqlplus and has excellent tools for browsing the database and debugging.

If you don’t have access to SQL Developer, you can format the sqlplus output as follows.

.. code-block:: sql
    :linenos:

    set linesize 400
    set pagesize 1000

    -- Run a separate command for each column to be displayed, as follows.
    -- a20 means the column is 20 characters wide. Use this format for varchar2 fields.
    -- For numbers, each digit is represented by a 9

    column OBJECT_NAME format a20
    column OBJECT_TYPE format a20
    column STATUS format a20
    column PRICE format 999.99

