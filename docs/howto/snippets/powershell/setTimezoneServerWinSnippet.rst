.. _setTimezoneWinSnippet:

===================
How To Set Timezone Server
===================

.. code-block:: powershell
    :linenos:

    w32tm /query /status

    Write-Host "Setting timezone server as {timezoneServer.fqn}"

    w32tm /config /syncfromflags:manual `
        /manualpeerlist:"{timezoneServer.fqn}" `
        /reliable:yes /update

If you get the error when running the above command at line 1:
:code:`The following error occurred: The service has not been started. (0x80070426)`
Run these commands:

.. code-block:: powershell
    :linenos:

    w32tm /unregister
    w32tm /register
    net start w32time

