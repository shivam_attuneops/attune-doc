.. _shutdownWindows:

========================
How To Shutdown Windows Snippet
========================

To shutdown a Windows VM:

.. code-block:: powershell
    :linenos:

    $WAIT = 10
    shutdown /s /t $WAIT /c "Shutdown from Attune"
    Write-Host "Shutting down in $WAIT seconds."

