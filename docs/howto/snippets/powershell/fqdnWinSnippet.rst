.. _fqdnWindows:

===============================================
How To Get Fully Qualified Domain Name Windows Snippet
===============================================

To get the FQDN for the current host:

.. code-block:: powershell
    :linenos:

    [System.Net.Dns]::GetHostByName(($env:computerName))

