.. _start_attune_local_user:

====================================
How To Start Attune From Local User Snippet
====================================

.. code-block:: powershell
    :linenos:

    $action = New-ScheduledTaskAction `
        -WorkingDirectory 'C:\Users\{winUser.user}' `
        -Execute `
        'C:\Users\{winUser.user}\AppData\Local\Programs\attune\resources\app.asar.unpacked\py\attune-server'


    $task = New-ScheduledTask -Action $action `

    Register-ScheduledTask 'attune-server' -InputObject $task `
       -User "{winUser.user}" `
       -Password "{winUser.password}"

    Start-ScheduledTask 'attune-server'

    Start-Sleep -Seconds 5

    # Check that its running
    Get-Process -Name "attune-server"

