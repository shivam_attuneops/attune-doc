.. _restartWindows:

=================================
How To Restart a Windows VM Code Snippet
=================================

To restart a Windows VM:

.. code-block:: powershell
    :linenos:

    $WAIT = 10
    shutdown /r /t $WAIT /c "Restart from Attune"
    Write-Host "Restarting in $WAIT seconds."

