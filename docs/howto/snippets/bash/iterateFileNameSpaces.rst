.. _iterateFileNameSpaces:

==================================
How To Iterate Over FileNames With Spaces
==================================

This snippet will iterate over file names containing spaces in a directory.

.. code-block:: bash
    :linenos:

    echo /my/**/dir | while IFS= read -r file
    do
        echo "Doing some work with ${file}"
        ls "${file}"
    done

This snippet will iterate over files that contain a string found by grep.

.. code-block:: bash
    :linenos:

    SCHEMA="newschema"
    function listFiles {
        grep -lRi OLDSCHEMA \
           /dir1 \
           /dir2
    }

    listFiles | while IFS= read -r file
    do
        echo "Replacing OLDSCHEMA with ${SCHEMA^^} in ${file}"
        # ^ upercases the first letter, ^^ upper cases them app
        sed-i "s/CORPDBA/${SCHEMA^^}/g" "${file}"
    done
