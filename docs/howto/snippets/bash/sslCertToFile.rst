.. _sslCertToFile:

==================================================
How To Perform SSL Handshake and Save Certificate to File
==================================================

The following example will retrieve the SSL certificate from a server and save it to
a file named :code:`out.cer`.

.. code-block:: bash
    :linenos:

    # Define ip and port

    openssl s_client -connect ${ip}:${port} 2>/dev/null </dev/null | openssl x509 -out out.cer
