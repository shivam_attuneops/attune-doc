.. _filterToFile:

==============================
Filter and Save Output to File
==============================

The following example will write the complete output of a command to a file and print a filtered subset of this output
to STDOUT.

.. code-block:: bash
    :linenos:

    <command> | tee -a output_file.txt | grep <filter>
