.. _buildingLists:

==================================
Build a List of Items to Loop Over
==================================

Build a list with comments:

.. code-block:: bash
    :linenos:

    list=""
    list+=" git"            # version control
    list+=" vim"            # text editor
    list+=" hdparm"         # hard drive tools
    list+=" nmap"           # network debugging
    list+=" fail2ban"       # brute-force protection

    for F in ${list}
    do
        echo ${F}
        done

Or if you want to keep it simple:

.. code-block:: bash
    :linenos:

    list="
        git
        vim
        hdparm
        nmap
        youtube-dl
        fail2ban
    "

    for F in ${list}
    do
        echo ${F}
    done
