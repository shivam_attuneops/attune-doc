.. _iterateInstallRpm:

====================================================================
How To Iterate Over RPM Files List and Install RPM if not Already Installed
====================================================================

.. code-block:: bash
    :linenos:

    PACKAGE="sqlite-devel"

    cd "/tmp/${PACKAGE}"

    i=0
    # lists the RPMS in the directory
    for RPM in *
    do
        # remove the extension of the RPM
        RPM_NAME_ONLY=$(echo "${RPM}" |
            sed -e 's/\([^.]*\).*/\1/'
            -e 's/\(.*\)-.*/\1/'
        )

        if rpm -qa | grep "${RPM_NAME_ONLY}";
        then
            echo "${RPM_NAME_ONLY} is already installed."
        else
            echo "Installing ${RPM} offline."

            yum localinstall --assumeyes --cacheonly \
                --disablerepo=* "${RPM}"
        fi

        ((i+=1))
    done

Explanation of sed REGEX
------------------------

Reference is at https://www.unix.com/shell-programming-and-scripting/150883-remove-version-numbers-package-lists.html

Using :code:`sqlite-devel-3.7.17-8.el7_7.1.x86_64.rpm` as an example this is what the first stage sed does:


.. code-block::
    :linenos:

    echo "sqlite-devel-3.7.17-8.el7_7.1.x86_64.rpm" | sed -e 's/\([^.]*\).*/\1/'

Gives this output:

.. code-block::
    :linenos:

    sqlite-devel-3

In :code:`\([^.]*\).*`:

- The :code:`[^.]*` matches everything that is not a :code:`.` zero or more times.

- The :code:`\([^.]*\)` matches everything that is not a :code:`.` zero or more times and assigns the result to variable 1.

- The :code:`([^.]*\).` matches everything up to but the first :code:`.`.

- The last part :code:`.*` strips away the characters after the first :code:`.`.

- Finally the :code:`\1` substitutes the the match we got in variable 1.

----

The second stage sed:

.. code-block:: bash
    :linenos:

    echo "sqlite-devel-3.7.17-8.el7_7.1.x86_64.rpm" | \
      sed -e 's/\([^.]*\).*/\1/' \
      -e 's/\(.*\)-.*/\1/'

Gives this output:

.. code-block::
    :linenos:

    sqlite-devel

In :code:`(.*\)-.*`:

- :code:`(.*\)-` matches everything before the first :code:`-` and stores the result into variable 1.

- :code:`-.*` strips away everything after the first :code:`-`.

- Finally the :code:`\1` substitutes the the match we got in variable 1.
