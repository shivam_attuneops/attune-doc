.. _bash_snippet:

====
Bash Script Snippets
====

.. toctree::
    :maxdepth: 1
    :caption: Contents:

    iterateFilesDirectory
    forLoopSpaces
    buildingLists
    iterateFileNameSpaces
    conditionalBash
    sslCertToFile
    appendToFile
    grepStdErr
    filterToFile
    exitInFunction
    pipeToFunction
    iterateInstallRpm
    selinuxEnabled
    rpmOffline
