.. _exitInFunction:

=======================
Exit Codes in Functions
=======================

Don't use :code:`exit` in bash functions, they should instead use :code:`return` so that the calling script has the
option of what to do on failure.

If :code:`set -o errexit` is set then the script will still exit with failure, but if the calling script doesn't want that to
happen then they can add :code:`myFunc || true` to stop it happening.

.. code-block:: bash
    :linenos:

    function bad {
      echo "# bye"
      return 1
    }
    function good {
      echo "# hi"
      return 0
    }

Output:

.. code-block:: bash
    :linenos:

    bad; echo "# $?"
    # bye
    # 1

    good; echo "# $?"
    # hi
    # 0
