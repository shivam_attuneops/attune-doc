.. _appendToFile:

=========================================
How To Append Lines To File If They Do Not Exist
=========================================

.. code-block:: bash
    :linenos:

    FILE=/etc/samba/smb.conf

    LINES=$(cat <<EOF
    [scan]
    Comment = Scans
    Path = /var/scan
    Browseable = yes
    Writeable = Yes
    only guest = no
    guest ok = no
    create mask = 0777
    directory mask = 0777
    Public = no
    EOF
    )

    grep -q "${LINES}" ${FILE} && echo "Nothing to do" || \
        echo "${LINES}" | tee -a ${FILE}
