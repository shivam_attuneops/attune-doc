.. _rpmOffline:

============================
How To Download RPM For Offline Use
============================

Here is an example usage of dnf to download cabextract and all it’s dependencies.

.. code-block:: bash
    :linenos:

    dnf download --resolve --alldeps cabextract
