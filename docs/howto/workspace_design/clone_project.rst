.. _clone_project:

==================
How To Clone a GIT Project
==================

Objective
---------

This procedure provides instructions to clone a project into an instance of
Attune.

Procedure
---------

Clone a GIT Repository into Attune
``````````````````````````````````

Start the Clone process in Attune.

#.  Open the Design Workspace

#.  Open the Project options dropdown

#.  Select Clone

.. image:: clone_project_01.png

----

Paste the GIT repository URL into Attune.

#.  Paste the URL

#.  Select Clone

.. image:: clone_project_02.png

.. note:: If the Download Large Files option is enabled, Attune will attempt
    to download files that have a URL to their source.

Complete
--------

This procedure is now complete. Now that this project is in your Attune
instance you can begin creating Jobs.
