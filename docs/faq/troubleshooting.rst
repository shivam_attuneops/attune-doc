.. _troubleshooting:

===============
Troubleshooting
===============

This page contains advice about errors and problems commonly encountered.

Attune Server and Web Application
---------------------------------

Logs
````

Inspect the Attune log:

::
    
    less /home/attune/attune.home/attune.log`

Inspect the system logs:

::
    
    sudo journalctl -f

System Disk Space Usage
```````````````````````

List filesystems

::
    
    df -h

List large files in the `/home/` directory:

::
    
    find /home/ -type f -size +100M -exec du -h {} +

Restart the Attune Service
``````````````````````````

Restart the attune service and tail the log.

::

    sudo systemctl restart attune
    tail -F ~/attune/attune.home/attune.log

Monitor the log.

Attune Application on Windows
-----------------------------

Logs
````

Inspect the Attune log:

::
    
    C:\Users\{account_name}\AppData\Local\com.servertribe.attune\attune.log

Attune Application on macOS
---------------------------

Logs
````

Inspect the Attune log:

::
    
    /Users/{account_name}/Library/Application Support/com.servertribe.attune/attune.log

Step Errors
-----------

HTTPSConnectionPool
```````````````````

The following error may occur if the network latency is high:

::

    HTTPSConnectionPool(host='{targetNodeIp}', port=5986): Max retries exceeded with url: /wsman (Caused by ConnectTimeoutError(<urllib3.connection.HTTPSConnection object at 0x1162b8e20>, 'Connection to {targetNodeIp} timed out. (connect timeout=5)'))


The error message you're seeing is related to a network connection issue. Attune is attempting to establish an HTTPS connection using a connection pool. Connection pools are used to manage multiple network connections efficiently.

Try re-running the step after 1 minute.
