.. _troubleshoot_ftpes:

===========================
How to Troubleshoot Git FTP
===========================

Verbose Flags
~~~~~~~~~~~~~

When troubleshooting the git-ftp command, the first step is to either add the :code:`-v` or :code:`-vv` flag to the
command. This will cause the commands output to be more verbose.

 * :code:`-v` Increase the output to be more verbose.

 * :code:`-vv` Is more verbose that the :code:`-v` flag.

Firewall Rules
~~~~~~~~~~~~~~

Git FTP will attempt to connect the port you specify, (FTP is port 21 by default) to the FTP server.

* **Active FTP**: The client initiates a command connection from a random unprivileged port (greater than 1023) to the
  FTP server's command port (21). Then, the client starts listening on its original port + 1 and sends this port number
  to the FTP server, which initiates a data connection from its port 20 to this port. So, the client's firewall needs to
  allow outbound traffic on port 21 and inbound traffic on random unprivileged ports for the data connection.

* **Passive FTP**: The client initiates a command connection from a random unprivileged port to the FTP server's command
  port (21). When the client needs data, it sends a PASV command over the command channel. The server replies with its
  chosen unprivileged port (greater than 1023), and the client initiates a data connection to this port. So, the
  client's firewall needs to allow outbound traffic on port 21 and also on random unprivileged ports for the data
  connection.

TLS Handshake
~~~~~~~~~~~~~

A successful TLS handshake between the client and server is essential for the FTPES protocol. During the handshake, the
client and server acknowlege each other, exchange certificates and keys, and agree on a cipher.

If there is a reason that the handshake is not successful, the connection will close.
The most common problem is when the client can not verify the certificate. When the :code:`--insecure` flag is set for
the git_ftp command, it will not attempt to verify the certificate.

Below is an example of a successful TLS handshake.

.. code-block::

    * TLSv1.3 (IN), TLS handshake, Server hello (2):
    { [88 bytes data]
    * TLSv1.3 (OUT), TLS change cipher, Change cipher spec (1):
    } [1 bytes data]
    * TLSv1.3 (OUT), TLS handshake, Client hello (1):
    } [588 bytes data]
    * TLSv1.3 (IN), TLS handshake, Server hello (2):
    { [161 bytes data]
    * TLSv1.3 (IN), TLS handshake, Encrypted Extensions (8):
    { [6 bytes data]
    * TLSv1.3 (IN), TLS handshake, Finished (20):
    { [52 bytes data]
    * TLSv1.3 (OUT), TLS handshake, Finished (20):
    } [52 bytes data]
    * SSL connection using TLSv1.3 / TLS_AES_256_GCM_SHA384 / [blank] / UNDEF
    * Server certificate:
    *  subject: C=NZ; ST=CAN; L=CHC; O=Synerty; OU=DevOps; CN=ftp.local
    *  start date: Dec 14 04:35:29 2023 GMT
    *  expire date: Dec  9 04:35:29 2043 GMT
    *  issuer: C=NZ; ST=CAN; L=CHC; O=Synerty; OU=DevOps; CN=ftp.local
    *  SSL certificate verify result: self signed certificate (18), continuing anyway.
    *   Certificate level 0: Public key type ? (2048/112 Bits/secBits), signed using sha256WithRSAEncryption
    { [5 bytes data]
    * TLSv1.3 (IN), TLS handshake, Newsession Ticket (4):

To check to see if the servers certificate can be verified, use:

.. code-block::
    :linenos:

    openssl s_client -connect hostname:port

Client vs Server Certificates
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In FTP active mode, the server will try and establish a TCP/TLS connection to the client for the data connection,
in this case, the server will expect a certificate from the client.

In FTP passive mode, the client is responsible for making the command and data connections and the server will not
request a clients certificate.

This behaviour can be seen when running the git_ftp command in very verbose mode.
