import os
import shutil

from setuptools import find_packages, setup

pip_package_name = "synerty-synerty_attune"
package_version = "2.1.0"

egg_info = "%s.egg-info" % pip_package_name
if os.path.isdir(egg_info):
    shutil.rmtree(egg_info)

if os.path.isfile("MANIFEST"):
    os.remove("MANIFEST")


# Force the dependencies to be the same branch
reqVer = ".".join(package_version.split(".")[0:2]) + ".*"

requirements = []

doc_requirements = [
    "Sphinx",
    "Sphinx_rtd_theme",
    "sphinx-autobuild",
]

requirements.extend(doc_requirements)

setup(
    name=pip_package_name,
    packages=find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
    install_requires=requirements,
    zip_safe=False,
    version=package_version,
    description="Synerty Attune - Public documentation package",
    author="Synerty",
    author_email="contact@synerty.com",
    url="https://bitbucket.org/synerty/synerty-attune",
    download_url="https://bitbucket.org/synerty/synerty-attune/get/%s.zip"
    % package_version,
    keywords=["Attune", "Python", "Platform", "synerty"],
    classifiers=[
        "Programming Language :: Python :: 2.7",
    ],
)
